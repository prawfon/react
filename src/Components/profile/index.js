import React,{ Component } from 'react';
import { Modal, Button } from 'antd';
const KEY_USER_DATA = 'user_data';
// function getEmail() {
//     const jsonStr = localStorage.getItem(KEY_USER_DATA);
//     return JSON.parse(jsonStr).email;
// }

class Profile extends Component {
    state = {
        email: '',
        isShowDialog: false
    }
    componentDidMount() {
        const jsonStr = localStorage.getItem(KEY_USER_DATA)
        this.setState({ email: JSON.parse(jsonStr).email })
    }
    showDialogConfirmLogout = () => {
        this.setState({ isShowDialog: true })
    }
    handleCancel = () => {
        this.setState({ isShowDialog: false })
    }
    handleOk = () => {
        localStorage.setItem(KEY_USER_DATA, JSON.stringify(
            {
                isShowDialog: false
            }
        ))
        this.props.history.push('/')
    }


    render() {
        return (
            <div>
                <h1>Email : {this.state.email}</h1>
                <button 
                    style={{
                        padding: '16',
                        backgroud: 'pink',
                        color: 'black',
                        fontSize: '18px',
                        cursor: 'pointor',
                        
                    }}
                    onClick={this.showDialogConfirmLogout}>Logout</button>
                <Modal
                    title="Confirm"
                    visible={this.state.isShowDialog}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    <p>Are you sure to logout?</p>
                   
                </Modal>
            </div>
        );
    }
}


export default Profile;